
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class op_booleanas {
    public static void main(String args[]){
         int op;
        op=Integer.parseInt(JOptionPane.showInputDialog( null,"Elige una opcion\n" +
                "1.- NOT\n" +
                "2.- AND\n"+                
                "4.- OR\n" +
                "5.- NOT, después OR\n" +
                "6.- OR, después NOT\n"+
                "7.- AND, después OR\n" + 
                "8.- Ejemplo del pagina 23 (Y ∧ S) ∨ (X ∧ ¬S)"));
        
         switch(op){ 
                case 1: 
                    
                    int[] a1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese su número binario")));
                    System.out.print("La negación de ");
                     for (int item : a1) {
                        System.out.print(item);
                    }
                     
                    System.out.print(" es: ");
                     
                     
                    int[] resultado = invertir(a1);
                     
                     for (int item : resultado) {
                        System.out.print(item);
                    }
                     
                     System.out.println();
                break;   
                    
                case 2:
                    int[] x1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número binario")));
                    int[] y1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número binario")));
                    
                    System.out.println("Operación AND ");
                    System.out.print("Primer valor: \t");
                     for (int item : x1) {
                        System.out.print(item);
                    }  System.out.println();
                    System.out.print("Segundo valor: \t");
                    for (int item : y1) {
                        System.out.print(item);
                    }System.out.println();
                    System.out.print("Resultado: \t");
                    int [] result_and = AND(x1,y1);
                     for (int item : result_and) {
                        System.out.print(item);
                    } System.out.println();
                break;
                    
                case 3:
                     
                    int[] x2 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número binario")));
                    int[] y2 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número binario")));
                    
                    System.out.println("Operación OR ");
                    System.out.print("Primer valor: \t");
                     for (int item : x2) {
                        System.out.print(item);
                    }  System.out.println();
                    System.out.print("Segundo valor: \t");
                    for (int item : y2) {
                        System.out.print(item);
                    }System.out.println();
                    System.out.print("Resultado: \t");
                    
                    int [] result_or = OR(x2,y2);
                     for (int item : result_or) {
                        System.out.print(item);
                    } System.out.println();
                    
                    
                    break;
                    
                case 5:
                     
                    int[] x3 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número binario")));
                    int[] y3 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número binario")));                    
                    
                    System.out.print("La negación del primer numero ");
                     for (int item : x3) {
                        System.out.print(item);
                    }
                     
                    System.out.print(" es: ");
                                          
                    int[] negacion = invertir(x3);
                     
                     for (int item : negacion) {
                        System.out.print(item);
                    }                     
                     System.out.println();                   
                     System.out.println("La operación OR entre la negación del primer número y el segundo número es:");
                     System.out.print("¬ del primer número \t");                     
                     
                     for (int item : negacion) {
                        System.out.print(item);
                    } System.out.println();
                     System.out.print("     Segundo número \t");  
                     for (int item : y3) {
                        System.out.print(item);
                    } System.out.println();
                     System.out.print("          Resultado \t");
                     int [] result3 = OR(negacion,y3);
                     for (int item : result3) {
                        System.out.print(item);
                    } System.out.println();
                    break;
                    
                case 6:
                     int[] x4 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer número binario")));
                     int[] y4 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo número binario")));
                    
                     System.out.println("Operación OR ");
                    System.out.print("Primer valor: \t");
                     for (int item : x4) {
                        System.out.print(item);
                    }  System.out.println();
                    System.out.print("Segundo valor: \t");
                    for (int item : y4) {
                        System.out.print(item);
                    }System.out.println();
                    System.out.print("Resultado: \t");
                    
                    int [] result_or2 = OR(x4,y4);
                     for (int item : result_or2) {
                        System.out.print(item);
                    } System.out.println();
                    
                     System.out.println("Y la negación del resultado es");
                    int [] result_inv = invertir(result_or2);
                    for (int item : result_inv) {
                        System.out.print(item);
                    } System.out.println();
                    break;
                    
                case 7:
                   
                    int[] x_1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de x en número binario")));
                    int[] y_1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de y en número binario")));
                    int[] z_1 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de z en número binario")));
                    
                    
                    System.out.print("x: \t");
                     for (int item : x_1) {
                        System.out.print(item);
                    }  System.out.println();
                    
                    System.out.print("y: \t");
                     for (int item : y_1) {
                        System.out.print(item);
                    }  System.out.println();
                    
                    System.out.print("z: \t");
                     for (int item : z_1) {
                        System.out.print(item);
                    }  System.out.println();
                    
                    System.out.println("operación AND entre los valores 'y' y 'z'");
                    
                     System.out.print("           y:\t");
                     for (int item : y_1) {
                        System.out.print(item);
                    }  System.out.println();
                     System.out.print("           z:\t");
                     for (int item : z_1) {
                        System.out.print(item);
                    }  System.out.println();
                    System.out.print("Resultado AND: \t");
                    
                    int [] result_and1 = AND(y_1,z_1);
                     for (int item : result_and1) {
                        System.out.print(item);
                    } System.out.println();
                    System.out.println("Operación OR entre 'x' y el resultado AND de 'y' y 'z'");
                    
                    System.out.print("           x:\t");
                     for (int item : x_1) {
                        System.out.print(item);
                    }  System.out.println();
                     System.out.print("Resultado AND:\t");
                     for (int item : result_and1) {
                        System.out.print(item);
                    }  System.out.println();
                    System.out.print(" Resultado OR: \t");
                    
                    int [] result_or3 = OR(x_1,result_and1);
                     for (int item : result_or3) {
                        System.out.print(item);
                    } System.out.println();
                    
                    
                    break;
                    
                case 8:
                    
                     
                    int[] x_2 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de x en número binario")));
                    int[] y_2 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de y en número binario")));
                    int[] s_2 = rellenar(Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor de z en número binario")));
                    
                    int [] and1 = AND(y_2,s_2);
                    int [] neg = invertir(s_2);
                    int [] and2 = AND(x_2,neg);
                    int []result_1 = OR(and1,and2);
                    
                    System.out.print("El resultado es : ");
                     for (int item : result_1) {
                        System.out.print(item);
                    } System.out.println();
                    break;
                    
         default: 
                System.out.print("\neleccion incorrecta" );

        }             
    }
    
    
     public static int[] rellenar (int numero){
        String binario = "";
        binario = numero + binario;
        
       
        
        int resultado [] = new int[8];
        for(int i = resultado.length -1, j=0, k=0; i > -1; i--, j++) {
            
            if(i >= binario.length()) {
                resultado[j] = 0;
            } else {
                
                resultado[j] = Integer.parseInt(binario.charAt(k)+"");
                k++;
            }
        }return resultado;
                
    }
     
     public static int[] invertir(int a[]) {
         
         
        int[] c = new int[a.length];
       
        for (int i = a.length - 1; i > -1; i--) {            
            int invert = a[i]; 
            if(invert == 0) {
               c[i] = 1;
               
           }
            
            
            
            else if(invert == 1) {
               c[i] = 0;
           }
           
           
          
        }
        return c;//Se regresa el arreglo.
        
     }
     
     public static int[] AND (int a[], int b[]){
         int[] c = new int[a.length];
          
         for (int i = 0 ; i < a.length; i++) { 
              int mult = a[i] * b[i];
              c[i] = mult;              
         }
         
         return c;
         
     }
     
          
     public static int[] OR (int a[], int b[]){
         int[] c = new int[a.length];
          
         for (int i = 0 ; i < a.length; i++) { 
              int sum = a[i] + b[i];
              
              if (sum==0){
                  c[i] = 0;   
              }else if(sum == 1 || sum == 2 ){
                  c[i] = 1; 
              }
                  
                         
         }
         
         return c;
         
     }
}
